#!/usr/bin/env node

const http=require('http');

http.createServer((req,res)=>{

  if(req.url='/'){
     let html='<html><head><meta charset="utf-8"></head><body><h1>hello</h1></body></html>';
     res.setHeader('Content-Type','text/html');
     res.setHeader('Content-Length',`${Buffer.from(html).byteLength}`);
     res.end(html);
                      
  }else{
     res.statusCode=404;
     res.end('ok!');
              
  }
}).listen(3000);

