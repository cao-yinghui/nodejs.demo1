#!/usr/bin/env node

const http = require('http');
const fs = require('fs');
const cheerio = require('cheerio'),
{join} = require('path');

http.createServer((req,res)=>{
  if(req.url === '/'){
    const file = fs.readFileSync(join(__dirname,'news.html')).toString()//读取new.html内容
    const $ = cheerio.load(file)//cheerio加载
    let result = []
  $('.model-details .news-model').each((item,i)=>{
    let createTime = $(i).children('span').text()
    if(createTime.search("2018") !== -1){
      const resulti = {
        newTitle:$(i).children('a').children('b').text(),
        createTime:createTime.substr(1,10)
      }
      result.push(resulti)
    }
  })
  res.setHeader('Content-Type','text/plain;charset=utf-8')
  let jg = JSON.stringify(result)
  res.end(jg)
  }else{
    res.end('获取失败')
  }


}).listen(8081);
