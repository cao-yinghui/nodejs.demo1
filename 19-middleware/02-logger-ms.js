#!/usr/bin/env node

const Koa = require('koa');

const app = new Koa()//实例化一个新的对象
app.use(async (ctx,next)=>{//M-logger
  next();
  const cost = ctx.cost;
  //const cost = await next();
  //const cost = ctx.response.header.cost;
  console.log(`${ctx.method} ${ctx.path}`);
  

})
app.use((ctx,next)=>{//M-ms
  const start = Date.now();
  next();
  const end = Date.now();
  ctx.set('cost',end-start);
 // ctx.header.cost = end-start;
  //console.log(`cost；${end-start}`)
})
app.use((ctx,next)=>{//M-hello
  ctx.body='hello world'
})
app.listen(3000);
console.log('hello world');
