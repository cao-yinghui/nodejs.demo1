#!/usr/bin/env node

const Koa = require('koa');

const app = new Koa()//实例化一个新的对象
app.use((ctx,next)=>{//M-logger
  console.log(`${ctx.method} ${ctx.path}`);
  next();
})
app.use((ctx,next)=>{//M-hello
  ctx.body='hello world'
})
app.listen(3000);
console.log('hello world');
