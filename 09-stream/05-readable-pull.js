#!/usr/bin/env node

const {Readable} = require('stream');
//==
//const Readable = require('stream').Readable;
const rs =new Readable();
let c ='a'.charCodeAt(0);
rs._read = () =>{
  rs.push(String.fromCharCode(c++));
  if(c==='z'.charCodeAt(0)+1)rs.push(null);
}

rs.pipe(process.stdout);
