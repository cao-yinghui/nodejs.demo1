#!/usr/bin/env node

const { Writable } = require ('stream');

class GreenStream extends Writable{

  constructor(){
    super();
  }
  _write(){
 process.stdout.write('\033[1;32m'+'hello world'+'\033[1;37m]');   
}
let ws = new GreenStream();

process.stdin.pipe(ws);
