#!/usr/bin/env node

const {Readable} =require('stream');

const rs = new Readable();

rs.push('hello');
rs.push('world');
rs.push(null);

rs.pipe(process.stdout);
