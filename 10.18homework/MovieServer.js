#!/usr/bin/env node

const fs = require('fs');
const http = require('http');
const data = require('./data'),
{join}=require('path');

http.createServer((req,res)=>{
  let url = req.url
  if(url === '/'){
    const file = fs.readFileSync(join(__dirname,'index.html')).toString()/读取index.html内容
    res.end(file)

  }
  else if(url ==='/getlist'){
    data = data.filter(item =>{
      if(item.movieRange !== '暂无评分'){
        return item
      }
    })
    for(var i=0;i<data.length-1;i++){
        for(var j=0;j<data.length-1;j++){
          if(data[i+1].movieRange>data[i].movieRange){
            var tmp = data[i+1];
                data[i+1] = data[i];
                data[i] = tmp;
          }
        }
      }
    data = JSON.toString(data);
    res.end(data)
  }
  
}).listen(8080)
