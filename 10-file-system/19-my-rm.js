#!/usr/bin/env node
const fs = require('fs');

const join = require('path').join;

const src = process.argv[2];

if(fs.statSync(src).isFile())  fs.unlinkSync(src);

if(fs.statSync(src).isDirectory()) deleteDir(src);

function deleteDir(folder){
    const files = fs.readdirSync(folder);

    for(var i=0; i<files.length; i++){
      const file = join(folder,files[i]);//获得要操作的文件的路径信息
     
      if(fs.statSync(file).isFile()){
        fs.unlinkSync(file);
        continue;
      }
      if(fs.statSync(file).isDirectory()) deleteDir(file);
    }
    fs.rmdirSync(folder);
}

