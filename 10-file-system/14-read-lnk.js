#!/usr/bin/env node

const fs = require('fs');
const lnk = process.argv[2];
const src = fs.readlinkSync(lnk);
console.log(`${lnk}->${src}`);
