#!/usr/bin/env node
const fs = require('fs');
const file = process.argv[2];
//get file size 
let len = fs.statSync(file).size;

//->create buffer
let buf = Buffer.alloc(len);

//open
let fid = fs.openSync(file,'r');
//operate
fs.readSync(fid,buf,0,len,0);
console.log(buf.toString('utf8'));
//close
fs.closeSync(fid);
//底层API打印，很少用，太繁琐
