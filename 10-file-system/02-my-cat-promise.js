#!/usr/bin/env node

import { readFile  } from 'fs/promises';
async function main(){
  const file = process.argv[2];
  const content = await readFile(file,'utf8');
  console.log(content);
}
main();
