#!/usr/bin/env node
const fs = require('fs');
const src = process.argv[2];
const dst = process.argv[3];
fs.createReadStream(src).pipe(fs.createWriteStream(dst));

//用fs模块的流API实现文件复制

