#!/usr/bin/env node
const fs = require('fs');
const file = process.argv[2];
const rs = fs.createReadStream(file);
rs.on('error',(e=>{
  console.error(e.message);
}));
rs.pipe(process.stdout);

